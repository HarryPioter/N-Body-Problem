module;
#include <unordered_set>
#include <vector>
#include <tuple>

export module Particles;

import vec2;
import Particle;

using chrom::Particle;
using chrom::ParticleProperties;
using chrom::vec2f;
using std::unordered_set;
using std::vector;

namespace chrom
{
export class Particles
{
public:
  void add_particle(float mass, float radius, const vec2f& position, const vec2f& velocity = {0, 0});
  long long remove_colliding_particles();
  const vector<Particle>& get_partcles() const;
  vector<Particle>& get_particles();

private:
  unordered_set<int> id_removal_list;
  vector<Particle> particles;

  void collect_colliding_particles();
};
}

module : private;

vec2f get_velocity_after_inelastic_collision(const ParticleProperties& p1, const ParticleProperties& p2);
ParticleProperties calculate_particle_properties_after_collision(const Particle& p1, const Particle& p2);