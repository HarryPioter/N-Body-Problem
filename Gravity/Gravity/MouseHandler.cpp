module;
#include <tuple>

module MouseHandler;

using chrom::vec2f;

MouseHandler::MouseHandler(RightClickAction callback) : righ_click_callback(std::move(callback))
{
}

void MouseHandler::move_mouse(vec2f position)
{
  mouse_position = position;
  drag_end = position;
}

void MouseHandler::press_right_button()
{
  const auto drag_line = get_drag_line();
  righ_click_callback(mouse_position, drag_line);

  drag_start = {};
  drag_end = {};
}

void MouseHandler::press_left_button(vec2f press_position)
{
  drag_start = press_position;
}

void MouseHandler::depress_left_button(vec2f depress_position)
{
  drag_end = depress_position;
}

std::optional<std::pair<vec2f, vec2f>> MouseHandler::get_drag_line() const
{
  if (drag_start and drag_end) return std::make_pair(*drag_start, *drag_end);

  return {};
}
