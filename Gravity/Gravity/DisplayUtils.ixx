export module DisplayUtils;

import Particle;
import vec2;

export void draw_particle(const chrom::Particle& particle);
export void draw_drag_n_drop_line(chrom::vec2f start, chrom::vec2f end);

module :private;
void draw_body(const chrom::Particle&);
void draw_particle_velocity(const chrom::Particle&);
void draw_vector(chrom::vec2f, chrom::vec2f);
