module;
#include <functional>
#include <optional>
#include <tuple>

export module MouseHandler;

import vec2;

using chrom::vec2f;

export using DragLine = std::pair<vec2f, vec2f>;
export using RightClickAction = std::function<void(vec2f, std::optional<DragLine>)>;

export class MouseHandler
{
  RightClickAction righ_click_callback;
  vec2f mouse_position = vec2f{0.0f, 0.0f};
  std::optional<vec2f> drag_start;
  std::optional<vec2f> drag_end;

public:
  MouseHandler(RightClickAction callback);
  void move_mouse(vec2f position);
  void press_right_button();
  void press_left_button(vec2f press_position);
  void depress_left_button(vec2f depress_position);
  std::optional<DragLine> get_drag_line() const;
};
