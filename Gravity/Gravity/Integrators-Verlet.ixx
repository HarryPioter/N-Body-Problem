module;
#include <span>
#include <vector>

export module Integrators : Verlet;

import vec2;
import Particle;

using chrom::Particle;
using chrom::vec2f;
using std::vector;

export class Verlet
{
  vector<vec2f> accelerations;
  vector<vec2f> next_accelerations;
  void reset_buffer();

public:
  void integrate(std::span<Particle> particles, float time_step);
  void resize_buffers(int size);
};