module;
#include <GL/glut.h>
#include <cmath>
#include <numbers>
#include <ranges>

module DisplayUtils;

import Particle;
import Math;

namespace
{
auto linspace(double start, double end, int steps)
{
  const auto delta = 1.0 / steps;
  return std::views::iota(0) | std::views::take(steps + 1)
         | std::views::transform([=](int step) { return std::lerp(start, end, step * delta); });
}
}

void draw_particle(const chrom::Particle& particle)
{
  draw_body(particle);
  draw_particle_velocity(particle);
}

void draw_drag_n_drop_line(chrom::vec2f start, chrom::vec2f end)
{
  glColor3f(0, 1, 0);
  draw_vector(start, end);
}

void draw_body(const chrom::Particle& particle)
{
  glColor3f(0, 1, 0);
  glBegin(GL_POLYGON);
  for (auto a : linspace(0, 2 * std::numbers::pi, 32)) {
    const auto& properties = particle.properties;
    glVertex2f(properties.radius * Math::cos(a) + properties.position.x,
               properties.radius * Math::sin(a) + properties.position.y);
  }

  glEnd();
}

void draw_particle_velocity(const chrom::Particle& particle)
{
  glColor3f(1, 0, 0);
  const auto& properties = particle.properties;
  draw_vector(properties.position, properties.position + properties.velocity);
}

void draw_vector(chrom::vec2f start, chrom::vec2f end)
{
  glBegin(GL_LINES);
  glVertex2f(start.x, start.y);
  glVertex2f(end.x, end.y);
  glEnd();
}
