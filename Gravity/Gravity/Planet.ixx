export module Planet;

export struct Planet {
  float mass;
  float radius;
};

export double calculate_planet_radius(double mass);
