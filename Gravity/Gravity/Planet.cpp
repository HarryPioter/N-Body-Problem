module;
#include <cmath>
#include <numbers>

module Planet;

double calculate_planet_radius(double mass)
{
  return std::cbrt(3 * mass / (4 * 0.5f * std::numbers::pi));
}