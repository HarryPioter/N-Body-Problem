#include <GL/glut.h>
#include <algorithm>
#include <iostream>
#include <numbers>
#include <optional>

import Math;
import ParticleSystem;
import vec2;
import DisplayUtils;
import MouseHandler;
import Planet;

using chrom::vec2f;
using namespace std::numbers;

void update(int);
void keyboard(unsigned char key, int x, int y);
void mouse_motion(int x, int y);
void mouse(int button, int state, int x, int y);
void display();
vec2f get_velocity_vector(std::optional<DragLine>);
void create_new_particle(vec2f position, std::optional<DragLine> drag_line);

ParticleSystem prt_system;

const Planet small_planet{60, calculate_planet_radius(60)}, big_planet{700, calculate_planet_radius(700)},
    super_nova{6000, calculate_planet_radius(6000)};
Planet current = big_planet;

MouseHandler mouse_handler = MouseHandler(create_new_particle);

int main(int argc, char* argv[])
{
  Math::initialize();
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  glutInitWindowSize(1000, 1000);
  glutInitWindowPosition(50, 50);
  glutCreateWindow("Gravity");

  glClearColor(0, 0, 0, 1);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-500.0, 500.0, 500.0, -500.0, 0, 1);
  glutDisplayFunc(display);
  glutKeyboardFunc(keyboard);
  glutMotionFunc(mouse_motion);
  glutMouseFunc(mouse);
  glutTimerFunc(25, update, 0);
  glutMainLoop();
}

void update(int val)
{
  prt_system.check_for_collisions();
  prt_system.time_step(0.5);
  glutPostRedisplay();
  glutTimerFunc(25, update, 0);
}

void keyboard(unsigned char key, int x, int y)
{
  switch (key) {
  case '1':
    current = small_planet;
    std::cout << "planet ( m = " << small_planet.mass << " kg, r = " << small_planet.radius << " m) \n";
    break;
  case '2':
    current = big_planet;
    std::cout << "planet ( m = " << big_planet.mass << " kg, r = " << big_planet.radius << " m) \n";
    break;
  case '3':
    current = super_nova;
    std::cout << "planet ( m = " << super_nova.mass << " kg, r = " << super_nova.radius << " m) \n";
    break;
  case 'u':
    prt_system.add_particle(20'000, calculate_planet_radius(20'000), {0, 0}, {0, 0});
    prt_system.add_particle(big_planet.mass, big_planet.radius, {150, 0}, {0, 15});
    break;
  case 'c':
    prt_system.remove_particles();
    break;
  case 27:
    exit(0);
  }
}

void mouse_motion(int x, int y)
{
  const vec2f mouse_position = {static_cast<float>(x - 500), static_cast<float>(y - 500)};
  mouse_handler.move_mouse(mouse_position);
}

void mouse(int button, int state, int x, int y)
{
  const vec2f mouse_position = {static_cast<float>(x - 500), static_cast<float>(y - 500)};
  mouse_handler.move_mouse(mouse_position);

  switch (button) {
  case GLUT_LEFT_BUTTON:
    if (state == GLUT_DOWN)
      mouse_handler.press_left_button(mouse_position);
    else
      mouse_handler.depress_left_button(mouse_position);
    break;

  case GLUT_RIGHT_BUTTON:
    if (state == GLUT_DOWN) mouse_handler.press_right_button();
    break;
  }
}

void display()
{
  glClear(GL_COLOR_BUFFER_BIT);

  if (const auto drag_line = mouse_handler.get_drag_line(); drag_line) {
    const auto& [line_start, line_end] = *drag_line;
    draw_drag_n_drop_line(line_start, line_end);
  }

  auto draw = [](const chrom::Particle& p) {
    draw_particle(p);
  };
  std::ranges::for_each(prt_system.get_partcles(), draw);

  glFlush();
  glutSwapBuffers();
}

vec2f get_velocity_vector(std::optional<DragLine> drag_line)
{
  if (not drag_line) return vec2f{0.0f, 0.0f};

  const auto& [start, end] = *drag_line;
  return start - end;
}

void create_new_particle(vec2f position, std::optional<DragLine> drag_line)
{
  const auto velocity_vector = get_velocity_vector(drag_line);
  prt_system.add_particle(current.mass, current.radius, position, velocity_vector);
}
