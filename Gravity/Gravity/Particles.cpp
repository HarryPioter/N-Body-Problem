module;
#include <algorithm>
#include <cassert>
#include <cmath>
#include <iterator>
#include <tuple>
#include <vector>

module Particles;

import Planet;

using chrom::Particle;
using chrom::ParticleProperties;
using chrom::vec2f;
using std::distance;
using std::remove_if;
using std::vector;

namespace chrom
{
void Particles::add_particle(float mass, float radius, const vec2f& position, const vec2f& velocity)
{
  particles.emplace_back(velocity, position, mass, radius);
}

void Particles::collect_colliding_particles()
{
  for (size_t i = 0; i < particles.size(); ++i) {
    auto& part2 = particles[i];

    if (id_removal_list.contains(part2.id)) continue;

    for (auto j = i + 1; j < particles.size(); ++j) {
      auto& part1 = particles[j];

      const auto dist = chrom::distance(part1.properties.position, part2.properties.position);
      const auto particles_collided = dist < part1.properties.radius + part2.properties.radius;
      if (not particles_collided) continue;

      const bool first_particle_lighter = part1.properties.mass < part2.properties.mass;
      auto& removed_particle = first_particle_lighter ? part1 : part2;
      auto& remaining_particle = first_particle_lighter ? part2 : part1;
      const auto properties = calculate_particle_properties_after_collision(remaining_particle, removed_particle);

      remaining_particle.properties = properties;
      id_removal_list.insert(removed_particle.id);
    }
  }
}

long long Particles::remove_colliding_particles()
{
  collect_colliding_particles();

  if (id_removal_list.empty()) {
    return 0;
  }

  auto particleToRemove = [&](const Particle& particle) {
    return id_removal_list.contains(particle.id);
  };

  const auto [rm_begin, rm_end] = std::ranges::remove_if(particles, particleToRemove);

  const auto deleted_particles = std::distance(rm_begin, rm_end);
  particles.erase(rm_begin, rm_end);
  id_removal_list.clear();

  return deleted_particles;
}

const vector<Particle>& Particles::get_partcles() const
{
  return particles;
}

vector<Particle>& Particles::get_particles()
{
  return particles;
}
}

vec2f get_velocity_after_inelastic_collision(const ParticleProperties& p1, const ParticleProperties& p2)
{
  auto mass_sum = p1.mass + p2.mass;
  return p1.velocity * (p1.mass / mass_sum) + p2.velocity * (p2.mass / mass_sum);
}

ParticleProperties calculate_particle_properties_after_collision(const Particle& heavier_particle,
                                                                 const Particle& lighter_particle)
{
  assert(heavier_particle.properties.mass >= lighter_particle.properties.mass);
  const auto velocity_after_collision =
      get_velocity_after_inelastic_collision(heavier_particle.properties, lighter_particle.properties);
  const auto position = heavier_particle.properties.position;
  const auto combined_mass = heavier_particle.properties.mass + lighter_particle.properties.mass;
  const auto radius_after_collision = static_cast<float>(calculate_planet_radius(combined_mass));

  return ParticleProperties{velocity_after_collision, position, combined_mass, radius_after_collision};
}
