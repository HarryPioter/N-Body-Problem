module Math;

import std.core;

namespace Math
{
constexpr auto two_pi = 2 * std::numbers::pi;

double cos(double radians)
{
  auto reduced_radians = reduce_radians(radians);
  auto [index, fraction] = get_index_and_scaling_factor(reduced_radians, cosine.size());

  return std::lerp(cosine[index], cosine[index + 1], fraction);
}

double sin(double radians)
{
  auto sign = radians < 0 ? -1 : 1;
  auto reduced_radians = reduce_radians(radians);
  auto [index, fraction] = get_index_and_scaling_factor(reduced_radians, sine.size());

  return sign * std::lerp(sine[index], sine[index + 1], fraction);
}

void initialize()
{
  auto dx = two_pi / 1000;
  for (size_t i = 0; i < 1000; ++i) {
    cosine[i] = std::cos(i * dx);
    sine[i] = std::sin(i * dx);
  }
}
}

double reduce_radians(double radians)
{
  auto positive_rad = abs(radians);
  return fmod(positive_rad, Math::two_pi);
}

std::pair<int, double> get_index_and_scaling_factor(double radians, size_t precompiledTableSize)
{
  auto idx = radians / Math::two_pi * precompiledTableSize;
  double integral_part;
  auto fraction = modf(idx, &integral_part);
  auto index = static_cast<int>(integral_part);

  return std::make_pair(static_cast<int>(integral_part), fraction);
}
