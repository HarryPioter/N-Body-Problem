module Particle;

namespace chrom
{
ParticleProperties::ParticleProperties(const vec2f& v, const vec2f& p, float m, float r)
    : velocity(v), position(p), mass(m), radius(r)
{
}

int Particle::id_counter = 0;

Particle::Particle(const vec2f& v, const vec2f& p, float m, float r) : id(id_counter++), properties{v, p, m, r}
{
}

Particle::Particle() : id(-1), properties{{0, 0}, {0, 0}, -1, -1}
{
}

}
