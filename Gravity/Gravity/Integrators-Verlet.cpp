module;
#include <algorithm>
#include <iterator>
#include <numbers>
#include <span>
#include <vector>

module Integrators : Verlet;

import vec2;
import Particle;

using chrom::Particle;
using chrom::vec2f;

namespace
{
void calculate_accelerations(std::span<const Particle> particles, std::span<vec2f> output)
{
  for (size_t y = 0; y < particles.size(); ++y) {
    const auto& particle2 = particles[y];

    for (auto x = y + 1; x < particles.size(); ++x) {
      const auto& particle1 = particles[x];

      auto distance = chrom::distance(particle2.properties.position, particle1.properties.position);
      auto dist_vector = particle2.properties.position - particle1.properties.position;
      const auto angle = dist_vector.angle();
      const auto opposite_angle = angle + std::numbers::pi;
      auto partial_acceleration = 1.0f / (distance * distance);

      output[x] += vec2f{static_cast<float>(particle2.properties.mass * partial_acceleration * cos(angle)),
                         static_cast<float>(particle2.properties.mass * partial_acceleration * sin(angle))};

      output[y] += vec2f{static_cast<float>(particle1.properties.mass * partial_acceleration * cos(opposite_angle)),
                         static_cast<float>(particle1.properties.mass * partial_acceleration * sin(opposite_angle))};
    }
  }
}
}

void Verlet::resize_buffers(int size)
{
  accelerations.resize(size);
  next_accelerations.resize(size);
}

void Verlet::reset_buffer()
{
  std::ranges::fill(accelerations, vec2f{0, 0});
  std::ranges::fill(next_accelerations, vec2f{0, 0});
}

void Verlet::integrate(std::span<Particle> particles, float time_step)
{
  reset_buffer();
  calculate_accelerations(particles, accelerations);
  for (size_t i = 0; i < particles.size(); ++i) {
    auto& part = particles[i];
    part.properties.position += (part.properties.velocity + accelerations[i] * time_step * 0.5f) * time_step;
  }
  calculate_accelerations(particles, next_accelerations);
  for (size_t i = 0; i < particles.size(); ++i) {
    auto& part = particles[i];
    part.properties.velocity += (accelerations[i] + next_accelerations[i]) * 0.5f * time_step;
  }
}
