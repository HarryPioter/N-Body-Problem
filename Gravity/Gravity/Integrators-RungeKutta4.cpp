module;
#include <algorithm>
#include <cmath>
#include <numbers>
#include <span>
#include <vector>

module Integrators : RungeKutta4;

import Particle;
import vec2;

using chrom::Particle;
using chrom::vec2f;
using std::sin;
using std::vector;

void RungeKutta4::reset_state()
{
  std::ranges::fill(dvdt, derivatives{});
  std::ranges::fill(drdt, derivatives{});
}

void RungeKutta4::copy_initial_position(std::span<const Particle> particles)
{
  std::ranges::transform(particles, std::begin(position), [](auto particle) { return particle.properties.position; });
}

void RungeKutta4::move_particles(std::span<Particle> particles, int nth_derivative, float dt)
{
  auto prev_derivative_idx = nth_derivative - 1;
  for (size_t i = 0; i < particles.size(); ++i)
    particles[i].properties.position = position[i] + drdt[i][prev_derivative_idx] * dt;
}

void RungeKutta4::copy_velocities_to_drdt(std::span<const Particle> particles)
{
  for (size_t i = 0; i < particles.size(); ++i) {
    auto& dRdt = drdt[i];
    std::ranges::fill(dRdt, particles[i].properties.velocity);
  }
}

void RungeKutta4::resize_buffers(int size)
{
  dvdt.resize(size);
  drdt.resize(size);
  position.resize(size);
}

void RungeKutta4::calculate_nth_dvdt(std::span<const Particle> particles, int nth_derivative)
{
  for (size_t y = 0; y < particles.size(); ++y) {
    const auto& part2 = particles[y];

    for (auto x = y + 1; x < particles.size(); ++x) {
      const auto& part1 = particles[x];

      const auto dist_vector = part2.properties.position - part1.properties.position;
      const auto distance = static_cast<float>(dist_vector.magnitude());
      const auto angle = static_cast<float>(dist_vector.angle());
      const auto opposite_angle = static_cast<float>(angle + std::numbers::pi);

      dvdt[x][nth_derivative] += calculate_acceleration_toward_other_particle(part2.properties.mass, distance, angle);

      dvdt[y][nth_derivative] +=
          calculate_acceleration_toward_other_particle(part1.properties.mass, distance, opposite_angle);
    }
  }
}

void RungeKutta4::calculate_nth_drdt(std::span<const Particle> particles, int nth_derivative, float dt)
{
  auto prev_derivative_idx = nth_derivative - 1;
  for (size_t i = 0; i < particles.size(); ++i) {
    drdt[i][nth_derivative] += dvdt[i][prev_derivative_idx] * dt;
  }
}

void RungeKutta4::integrate(std::span<Particle> particles, float dt)
{
  reset_state();
  copy_initial_position(particles);

  copy_velocities_to_drdt(particles);
  calculate_nth_dvdt(particles, 0);

  const float time_steps[]{dt / 2, dt / 2, dt};
  for (int derivative = 1; derivative < 4; ++derivative) {

    move_particles(particles, derivative, time_steps[derivative - 1]);
    calculate_nth_dvdt(particles, derivative);
    calculate_nth_drdt(particles, derivative, time_steps[derivative - 1]);
  }

  get_integration_result(particles, dt);
}

void RungeKutta4::get_integration_result(std::span<Particle> particles, float dt)
{
  const auto coeff = dt / 6;
  for (size_t i = 0; i < particles.size(); ++i) {
    auto& current_particle = particles[i];
    const auto& dVdT = dvdt[i];
    current_particle.properties.velocity += (dVdT[0] + (dVdT[1] + dVdT[2]) * 2 + dVdT[3]) * coeff;
    const auto& dRdT = drdt[i];
    current_particle.properties.position = position[i] + (dRdT[0] + (dRdT[1] + dRdT[2]) * 2 + dRdT[3]) * coeff;
  }
}

vec2f calculate_acceleration_toward_other_particle(float other_mass, float distance, float angle)
{
  const auto squared_inverted_distance = 1.0f / (distance * distance);
  return vec2f{other_mass * squared_inverted_distance * cos(angle),
               other_mass * squared_inverted_distance * sin(angle)};
}
