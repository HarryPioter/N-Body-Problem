export module Math;

import std.core;

namespace Math
{
std::array<double, 1000> sine;
std::array<double, 1000> cosine;

export double cos(double radians);
export double sin(double radians);
export void initialize();
}

module : private;

double reduce_radians(double);
std::pair<int, double> get_index_and_scaling_factor(double, size_t);