module;
#include <array>
#include <span>
#include <vector>

export module Integrators : RungeKutta4;

import Particle;
import vec2;

using chrom::Particle;
using chrom::vec2f;
using std::array;
using std::vector;

using derivatives = array<vec2f, 4>;

export class RungeKutta4
{
  vector<derivatives> dvdt;
  vector<derivatives> drdt;
  vector<vec2f> position;

  void reset_state();
  void copy_initial_position(std::span<const Particle> particles);
  void move_particles(std::span<Particle>, int nth_derivative, float dt);
  void copy_velocities_to_drdt(std::span<const Particle> particles);
  void calculate_nth_dvdt(std::span<const Particle> particles, int nth_derivative);
  void calculate_nth_drdt(std::span<const Particle> particles, int nth_derivative, float dt);

public:
  void resize_buffers(int size);
  void integrate(std::span<Particle> particles, float dt);
  void get_integration_result(std::span<Particle> particles, float dt);
};

vec2f calculate_acceleration_toward_other_particle(float other_mass, float distance, float angle);
