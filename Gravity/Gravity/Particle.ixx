export module Particle;

import vec2;

export namespace chrom
{
struct ParticleProperties {
  vec2f velocity;
  vec2f position;
  float mass;
  float radius;

  ParticleProperties(const vec2f& v, const vec2f& p, float m, float r);
};

struct Particle {
private:
  static int id_counter;

public:
  int id;
  ParticleProperties properties;

  Particle(const vec2f& v, const vec2f& p, float m, float r);
  Particle();
};
}